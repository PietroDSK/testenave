function somar(){

    let outSomar = document.getElementById("outSomar")


    let pessoas = [
        {id: 1, nome: "juca", sobrenome: "da silva", idade:42},
        {id: 2, nome: "daniel", sobrenome: "gonçalves", idade: 21},
        {id: 3, nome: "matheus", sobrenome: "garcia", idade: 28},
        {id: 4, nome: "gabriel", sobrenome: "pinheiro", idade: 21}
    ]


   let pessoaSoma= pessoas.reduce((total, idade) => total += idade.idade, 0)

   outSomar.textContent = "Soma das Idades: " + pessoaSoma
  
   
}
const btnSomar = document.getElementById("btnSomar")
btnSomar.addEventListener("click", somar)